﻿/* Author Name: Michael Paxton
 * Purpose of Class: Code for displaying the invoice window with the correct values
 * Date last Modified: 22/10/2016
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Coursework1._1
{
    /// <summary>
    /// Interaction logic for Invoice.xaml
    /// </summary>
    public partial class Invoice : Window
    {
        public Invoice()
        {
            InitializeComponent();
        }
        public Invoice(string firstName, string secondName, string instiName, string instiAddress, string confName, double cost)
        {
            InitializeComponent();
            txtNameOut.Text = "" + firstName + " " + secondName;
            if (!string.IsNullOrEmpty(instiAddress))
                txtInstitutionOut.Text = "" + instiName + ", at the address: " + instiAddress;
            else
                txtInstitutionOut.Text = "" + instiName;
            txtConferenceName.Text = "" + confName;
            txtCost.Text = "£" + cost;            
        }
    }
}
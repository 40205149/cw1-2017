﻿/* Author Name: Michael Paxton
 * Purpose of Class: Code for displaying the certificate window with the correct values
 * Date last Modified: 22/10/2016
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Coursework1._1
{
    /// <summary>
    /// Interaction logic for Certificate.xaml
    /// </summary>
    public partial class Certificate : Window
    {
        public Certificate()
        {
            InitializeComponent();
        }
        public Certificate(string fstName, string sndName, string confName)
        {
            InitializeComponent();
            txtNameOut.Text = "" + fstName + " " + sndName;
            txtConferenceName.Text = "" + confName;
        }
        public Certificate(string fstName, string sndName, string confName, string paperTitle)
        {
            InitializeComponent();
            txtNameOut.Text = "" + fstName + " " + sndName;
            txtConferenceName.Text = "" + confName;
            lblPaperTitleOut.Content = paperTitle;
            lblPaperTitle.Content = "Paper Title:";           
        }
    }
}

﻿/* Author: Michael Paxton 
 * Main class of the program, majority of functions carried out here.
 * Date Last Modified: 22/10/16
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Coursework1._1
{   /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Global variable for attendee 1 and institution 1
        Attendee att1 = new Attendee();
        Institution inst1 = new Institution();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnSet_Click(object sender, RoutedEventArgs e)
        {        
            // Filling the values of attendee and institution with GUI values.
            att1.FirstName = txtFstName.Text;
            att1.SecondName = txtSndName.Text;

            int output = 0;
            bool result = Int32.TryParse(txtRefNum.Text, out output);
            att1.AttendeeRef = output;
            if (result)           
                Console.WriteLine("successfully converted '{0}' to {1}.", txtRefNum.Text, output);            
            else           
                Console.WriteLine("Conversion failed");           

            inst1.InstitutionName = txtInstiName.Text;
            inst1.InstitutionAddress = txtInstiAddress.Text;
            att1.ConferenceName = txtConName.Text;
            if (radRegOrganiser.IsChecked == true)            
                att1.RegistrationType = "Organiser";               
            else if (radRegFull.IsChecked == true)            
                att1.RegistrationType = "Full";            
            else if (radRegStudent.IsChecked == true)           
                att1.RegistrationType = "Student";            
                    
            //Setting the Paid and Presenter Variables
            if (radPaidYes.IsChecked == true)            
                att1.Paid = true;                           
            if (radPaidNo.IsChecked == true)            
                att1.Paid = false;                           
            if (radPresenterYes.IsChecked == true)           
                att1.Presenter = true;                          
            if (radPresenterNo.IsChecked == true)           
                att1.Presenter = false;                         
            att1.PaperTitle = txtPaperTitle.Text;
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            //When the "Clear" button is clicked, all of the form tools are emptied,
            //Leaving the class properties unchanged.
            txtFstName.Clear();
            txtSndName.Clear();
            txtRefNum.Clear();
            txtInstiName.Clear();
            txtInstiAddress.Clear();
            txtConName.Clear();
            radRegOrganiser.IsChecked = false;
            radRegFull.IsChecked = false;
            radRegStudent.IsChecked = false;
            txtPaperTitle.Clear();
            if (radPaidYes.IsChecked == true)
                radPaidYes.IsChecked = false;
            if (radPaidNo.IsChecked == true)
                radPaidNo.IsChecked = false;
            if (radPresenterYes.IsChecked == true)
                radPresenterYes.IsChecked = false;
            if (radPresenterNo.IsChecked == true)
                radPresenterNo.IsChecked = false;
        }

        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            //When this button is clicked, the form tools are filled with the values stored in the institution and attendee
            txtFstName.Text = att1.FirstName;
            txtSndName.Text = att1.SecondName;
            txtRefNum.Text = att1.AttendeeRef.ToString();
            txtInstiName.Text = inst1.InstitutionName;
            txtInstiAddress.Text = inst1.InstitutionAddress;
            txtConName.Text = att1.ConferenceName;
            if (!string.IsNullOrEmpty(att1.RegistrationType) && (att1.RegistrationType.Equals("Organiser")))            
                radRegOrganiser.IsChecked = true;             
            else if (!string.IsNullOrEmpty(att1.RegistrationType) && att1.RegistrationType.Equals("Full"))           
                radRegFull.IsChecked = true;          
            else if (!string.IsNullOrEmpty(att1.RegistrationType) && att1.RegistrationType.Equals("Student"))           
                radRegStudent.IsChecked = true;                 
            else            
                MessageBox.Show("No value is stored for 'Registration Type'.");            
            if (att1.Paid == true)           
                radPaidYes.IsChecked = true;                         
            if (att1.Paid == false)           
                radPaidNo.IsChecked = true;                           
            if (att1.Presenter == true)            
                radPresenterYes.IsChecked = true;
            if (att1.Presenter == false)            
                radPresenterNo.IsChecked = true;                           
            txtPaperTitle.Text = att1.PaperTitle;
        }

        private void btnInvoice_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(att1.FirstName) && !string.IsNullOrEmpty(att1.SecondName) && att1.AttendeeRef != 0 && (att1.RegistrationType == "Full" || att1.RegistrationType == "Student" || att1.RegistrationType == "Organiser"))
            {
                Invoice invOutput = new Invoice(att1.FirstName, att1.SecondName, inst1.InstitutionName, inst1.InstitutionAddress, att1.ConferenceName, att1.getCost(att1.RegistrationType));
                invOutput.ShowDialog();
            }
            else
                MessageBox.Show("ERROR: Please enter values before attempting to create an Invoice.");
                      
        }

        private void btnCertificate_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(att1.FirstName) && !string.IsNullOrEmpty(att1.SecondName) && att1.AttendeeRef != 0 && (att1.RegistrationType == "Full" || att1.RegistrationType == "Student" || att1.RegistrationType == "Organiser"))
            {
                if (!att1.Presenter)
                {
                    Certificate certOutput = new Certificate(att1.FirstName, att1.SecondName, att1.ConferenceName);
                    certOutput.ShowDialog();
                }
                else
                {
                    Certificate certOutput = new Certificate(att1.FirstName, att1.SecondName, att1.ConferenceName, att1.PaperTitle);
                    certOutput.ShowDialog();
                }
            }
            else
                MessageBox.Show("ERROR: Please enter values before attempting to create a Certificate.");           
        }

        public bool areChecked()
        {
            if (radPaidYes.IsChecked == true || radPaidNo.IsChecked == true)
                return true;
            else
                return false;
        }   
    
    }
}

﻿/* Author Name: Michael Paxton
 * Purpose of Class: Modify, validate and store data about the institution
 * Date last Modified: 21/10/2016
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework1._1
{
    class Institution
    {
        // private variable declaration
        private string institutionName;
        private string institutionAddress;

        public string InstitutionName // property for manipulating institution name
        {
            get { return institutionName; }
            set { institutionName = value; }
        }
        public string InstitutionAddress // property for manipulating institution address
        {
            get { return institutionAddress; }
            set { institutionAddress = value; }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//MessageBox.Show("" + att1.FirstName);
namespace Coursework1._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Global variable for attendee 1
        Attendee att1 = new Attendee();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            // Attendee att1 = new Attendee();
            att1.FirstName = txtFstName.Text;
            att1.SecondName = txtSndName.Text;
            att1.AttendeeRef = Convert.ToInt32(txtRefNum.Text);
            att1.InstitutionName = txtInstiName.Text;
            att1.ConferenceName = txtConName.Text;
            att1.RegistrationType = txtRegType.Text;
            att1.PaperTitle = txtPaperTitle.Text;
            if (radPaidYes.IsChecked == true)
                att1.Paid = true;  
            if (radPaidNo.IsChecked == true)
                att1.Paid = false;
            if (radPresenterYes.IsChecked == true)
                att1.Presenter = true;
            if (radPresenterNo.IsChecked == true)
                att1.Presenter = false;         
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            //When the "Clear" button is clicked, all of the form tools are emptied,
            //Leaving the class properties unchanged.
            txtFstName.Clear();
            txtSndName.Clear();
            txtRefNum.Clear();
            txtInstiName.Clear();
            txtConName.Clear();
            txtRegType.Clear();
            txtPaperTitle.Clear();
            if (radPaidYes.IsChecked == true)
                radPaidYes.IsChecked = false;
            if (radPaidNo.IsChecked == true)
                radPaidNo.IsChecked = false;
            if (radPresenterYes.IsChecked == true)
                radPresenterYes.IsChecked = false;
            if (radPresenterNo.IsChecked == true)
                radPresenterNo.IsChecked = false;
        }

        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            txtFstName.Text = att1.FirstName;
            txtSndName.Text = att1.SecondName;
            txtRefNum.Text = att1.AttendeeRef.ToString();
            txtInstiName.Text = att1.InstitutionName;
            txtConName.Text = att1.ConferenceName;
            txtRegType.Text = att1.RegistrationType;
            txtPaperTitle.Text = att1.PaperTitle;
            if (att1.Paid == true)
                radPaidYes.IsChecked = true;
            if (att1.Paid == false)
                radPaidNo.IsChecked = true;
            if (att1.Presenter == true)
                radPresenterYes.IsChecked = true;
            if (att1.Presenter == false)
                radPresenterNo.IsChecked = true;
        }

        private void btnInvoice_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("" + att1.FirstName + " " + att1.SecondName + " who belongs to the institution of: " + att1.InstitutionName + ", will be attending the conference: " + att1.ConferenceName + ". This will cost a total of: £");// + att1.getCost());
        }
    }
}

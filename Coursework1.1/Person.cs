﻿/* Author Name: Michael Paxton
 * Purpose of Class: Modify, validate and store data about the person
 * Date last Modified: 21/10/2016
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Coursework1._1
{
    class Person
    {
        private string firstName;
        private string secondName;

        public string FirstName // property for manipulating first name
        {
            get { return firstName; }
            set
            {
                if (string.IsNullOrEmpty(value) == true)
                    System.Windows.MessageBox.Show("ERROR: Please input a string into the 'First Name' box.");
                else
                    firstName = value;
            }
        }

        public string SecondName // property for manipulating second name
        {
            get { return secondName; }
            set
            {
                if (string.IsNullOrEmpty(value) == true)
                    MessageBox.Show("ERROR: Please input a string into the 'Second Name' box.");
                else
                    secondName = value;
            }
        }
    }
}
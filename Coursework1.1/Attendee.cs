﻿/* Author Name: Michael Paxton
 * Purpose of Class: Modify, validate and store data about the attendee
 * Date last Modified: 21/10/2016
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Coursework1._1
{
    class Attendee : Person
    {
        //Private Variable Declaration
        private int attendeeRef; 
        private string conferenceName;
        private string registrationType;
        private bool paid;
        private bool presenter;
        private string paperTitle;

        // property for manipulating attendee reference
        public int AttendeeRef 
        {             
            get { return attendeeRef; }           
            set //Exception handling, value must be < 60000 and be > 40000
            {
                try
                {
                    if (value > 60000)                    
                        throw new ArgumentException("ERROR: The value entered in the 'Attendee Reference' box is incorrect, please enter an integer between 40000 and 60000.");                   
                    if (value < 40000)                   
                        throw new ArgumentException("ERROR: The value entered in the 'Attendee Reference' box is incorrect, please enter an integer between 40000 and 60000.");                   
                    attendeeRef = value;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }                             
        }

        // property for manipulating conference name
        public string ConferenceName 
        {
            get { return conferenceName; }
            set { conferenceName = value; }
        }

        // property for manipulating registration type
        public string RegistrationType 
        {
            get { return registrationType; }
            set //Exception handling, value must be equal to 'full', 'student', or 'organiser'
            { 
                if (value.ToLower().Equals("full"))
                    registrationType = value;
                else if (value.ToLower().Equals("student"))
                    registrationType = value;
                else if (value.ToLower().Equals("organiser"))
                    registrationType = value;
                else
                    MessageBox.Show("ERROR: invalid entry present in the 'Registration Type' Box, please enter a valid string."); 
            }
        }

        // property for manipulating if the user has paid
        public bool Paid 
        {
            get { return paid; }
            set { paid = value; }
        }

        // property for manipulating if the user is a presenter
        public bool Presenter 
        {
            get { return presenter; }
            set { presenter = value; }
        }

        // property for manipulating the paper title
        public string PaperTitle 
        {
            get { return paperTitle; }
            set // Exception handling:
                // If presenter = true => papertitle must have a value
                // If presenter = false => papertitle must be empty
            {               
                if (presenter && !string.IsNullOrEmpty(value))                
                    paperTitle = value;                                                  
                else if (presenter && string.IsNullOrEmpty(value))                
                    System.Windows.MessageBox.Show("ERROR: No entry present in the 'Paper Title' box, please enter a string.");                                  
                else if (!presenter && string.IsNullOrEmpty(value))               
                    paperTitle = value;                                  
                else if (!presenter && !string.IsNullOrEmpty(value))                
                    System.Windows.MessageBox.Show("ERROR: Entry present in the 'Paper Title' box, please delete this string.");                                   
            }
        }

        // method for gathering cost dependent on registration type
        public double getCost(string regType) 
        {
            if (regType.Equals("Full"))
            {
                if (presenter)                
                    return (500.00 * 0.90);                
                else                
                    return 500.00;               
            }
            else if (regType.Equals("Student"))
            {
                if (presenter)               
                    return (300.00 * 0.90);               
                else               
                    return 300.00;               
            }
            else          
                return 0.00;            
        }

    }
}
